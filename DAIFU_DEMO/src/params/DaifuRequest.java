package params;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.digest.DigestUtils;

public class DaifuRequest {
	
	/**
	 * 商户ID。
	 */
	public String company_id;
	
	/**
	 * 商户订单号。
	 */
	public String merchant_order_no;
	
	/**
	 * 代付金额，必须大于0，包含2位小数。
	 */
	public String amount;
	
	/**
	 * 银行编码。参见附录中的银行编码列表。
	 */
	public String bank_code;
	
	/**
	 * 代付账户类型，可选值：银行卡(bank_card)、支付宝(Alipay)、微信（Wechat）、USDT-ERC20（Erc20）、USDT-TRC20（Trc20）、USDT-OMNI（Omni）
	 * 原始需求在 issues/586
	 * issues/940
	 */
	public String account_type;
	
	/**
	 * USDT加密币代付时传入的USDT收款地址
	 * issues/940
	 */
	public String usdt_address;
	
	/**
	 * 币种。
	 */
	public String currency;
	
	/**
	 * 收款人手机号。
	 */
	public String phone;
	
	/**
	 * 加密数据。内容： AES(姓名|卡号|身份证号)。AES加密的密钥为代付密钥。
	 */
	public String enc_data = "";
	
	/**
	 * 备注。
	 */
	public String comment = "";
	
	/**
	 * 签名类型。目前只能是MD5。
	 */
	public String sign_type = "MD5";
	
	/**
	 * 签名。
	 */
	public String sign;

	public SortedMap<String, String> getParams() {
		TreeMap<String, String> params = new TreeMap<>();

		params.put("company_id", this.company_id);
		params.put("merchant_order_no", this.merchant_order_no);
		params.put("amount", this.amount);
		params.put("account_type", this.account_type); // 增加代付账户类型 2021-04-28 issues/940
		params.put("usdt_address", this.usdt_address); // 增加加密币代付收款地址 2021-04-29 issues/940
		params.put("bank_code", this.bank_code);
		params.put("phone", this.phone);
		params.put("enc_data", this.enc_data);
		params.put("comment", this.comment);
		params.put("currency", this.currency);

		return params;
	}
	
	/**
	 * 计算签名。
	 * 
	 * @param tag
	 * @param secret
	 * @return
	 */
	public String sign(String tag, String secret) {
		return this.sign(tag, this.getParams(), secret);
	}
	
	/**
	 * 计算签名。
	 * 
	 * @param tag
	 * @param params
	 * @param secret
	 * @return
	 */
	public String sign(String tag, SortedMap<String, String> params, String secret) {
		StringBuilder sb = new StringBuilder();
		
		for (Map.Entry<String, String> entry : params.entrySet()) {
			if (entry.getValue() == null || entry.getValue().isEmpty() || 
					entry.getKey().equals("sign_type") || entry.getKey().equals("sign")) {
				continue;
			}
			sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		sb.append("key=").append(secret);
		
		String contentToSign = sb.toString();
		String sign = DigestUtils.md5Hex(contentToSign).toUpperCase();
		
		return sign;
	}

	/**
	 * 校验收到的签名。
	 * 
	 * @param tag
	 * @param sign
	 * @param params
	 * @param secret
	 * @return
	 */
	public boolean verifySign(String tag, String sign, SortedMap<String, String> params, String secret) {
		String ourSign = this.sign(tag, params, secret);
		boolean verifyOk = ourSign.equalsIgnoreCase(sign);
		if (!verifyOk ) {
			
			System.out.println("DAIFU failed verify signature " + tag + ", "
					+ "our sign: " + ourSign + ", sign received: " + sign);
		}
		return verifyOk;
	}
	
	/**
	 * 加密。
	 * 
	 * @param input
	 * @param key
	 * @return
	 */
    public static String encrypt(String input, String key) {
        byte[] crypted = null;

        try {
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(1, skey);
            crypted = cipher.doFinal(input.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
        	e.printStackTrace();
        	return null;
        }

        return Base64.getEncoder().encodeToString(crypted);
    }

    /**
     * 解密。
     * 
     * @param hexStr
     * @param key
     * @return
     */
    public static String decrypt(String hexStr, String key) {
        byte[] output = null;

        try {
            byte[] decByte = Base64.getDecoder().decode(hexStr);
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(2, skey);
            output = cipher.doFinal(decByte);
        } catch (Exception e) {
        	e.printStackTrace();
            return null;
        }

        return new String(output, StandardCharsets.UTF_8);
    }
    
}
