package main;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.UUID;

import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;

import params.DaifuRequest;

public class DaifuClient {
	public String key = "";
	public String url = "";

	public String toQueryString(Map<String, String> data) throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, String> entry : data.entrySet()) {
			sb.append(URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8.name()));
			sb.append("=");
			sb.append(URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8.name()));
			sb.append("&");
		}
		sb.setLength(sb.length() - 1);
		String ret = sb.toString();
		return ret;
	}

	public String daifuPost(Map<String, String> params) throws Exception {
		return this.sendDaifuRequest(this.toQueryString(params));
	}

	/**
	 * 发起代付请求。
	 * 
	 * @param DaifuJinmao 代付
	 * @param upstream
	 * @return
	 * @throws Exception
	 */
	protected String sendDaifuRequest(String data) throws Exception {
		String res;
		try {
			res = Request.Post(this.url).addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
					.bodyString(data, ContentType.APPLICATION_FORM_URLENCODED).connectTimeout(100000)
					.socketTimeout(100000).execute().returnContent().asString();

			System.out.println("Daifu request: " + data);
		} catch (Exception e) {
			System.out.println("Send failed request, url: " + url + ", body: " + data);
			e.printStackTrace();
			throw e;
		}

		return res;
	}

	// 原始请求参数
	public Map<String, String> makeOriginalRequestParams() {
		DaifuRequest request = new DaifuRequest();
		request.amount = "100";
		request.bank_code = "78";
		request.comment = "comment";
		request.company_id = "fb";
		request.enc_data = "张1三|6512436514256341265|612731198808012926"; // 姓名|卡号|身份证
		request.merchant_order_no = UUID.randomUUID().toString();
		request.phone = "13504508474";
		request.sign_type = "MD5";
		request.currency = "CNY";
		request.enc_data = DaifuRequest.encrypt(request.enc_data, key);
		request.sign = request.sign("daifu-request", key);

		SortedMap<String, String> params = request.getParams();
		params.remove("account_type");
		params.remove("usdt_address");
		params.put("sign", request.sign);
		params.put("sign_type", request.sign_type);
		return params;
	}

	// 法币请求参数
	public Map<String, String> makeCNYRequestParams(String account_type) {
		DaifuRequest request = new DaifuRequest();
		request.amount = "100";
		request.bank_code = "78";
		request.comment = "comment";
		request.company_id = "fb";
		request.enc_data = "张1三|6512436514256341265|612731198808012926"; // 姓名|卡号|身份证
		request.merchant_order_no = UUID.randomUUID().toString();
		request.phone = "13504508474";
		request.account_type = account_type;
		request.usdt_address = "";
		request.sign_type = "MD5";
		request.currency = "CNY";
		request.enc_data = DaifuRequest.encrypt(request.enc_data, key);
		request.sign = request.sign("daifu-request", key);

		SortedMap<String, String> params = request.getParams();
		params.put("sign", request.sign);
		params.put("sign_type", request.sign_type);
		return params;
	}

	// 加密币请求参数
	public Map<String, String> makeUSDTRequestParams(String account_type, String usdt_address) {
		DaifuRequest request = new DaifuRequest();
		request.amount = "100";
		request.bank_code = "";
		request.comment = "comment";
		request.company_id = "fb";
		request.enc_data = "张三||612731198808012926"; // 姓名|卡号|身份证
		request.merchant_order_no = UUID.randomUUID().toString();
		request.phone = "13504508474";
		request.account_type = account_type;
		request.usdt_address = usdt_address;
		request.sign_type = "MD5";
		request.currency = "USDT";
		request.enc_data = DaifuRequest.encrypt(request.enc_data, key);
		request.sign = request.sign("daifu-request", key);

		SortedMap<String, String> params = request.getParams();
		params.put("sign", request.sign);
		params.put("sign_type", request.sign_type);
		return params;
	}

	public static void main(String[] args) throws Exception {
		DaifuClient client = new DaifuClient();
//		client.url = "http://localhost:8777/api/v2/daifu/request";
		client.url = "https://api.acepay.net/api/v2/daifu/request";
		// 商户后台-系统管理-开发参数 中的代付密钥。
		client.key = "";

		// 原始代付请求
		String res = client.daifuPost(client.makeOriginalRequestParams());
		System.out.println("Daifu response: " + res);
		
		// 法币代付请求-银行卡（bank_card）
		String cnyDaifuRes = client.daifuPost(client.makeCNYRequestParams("bank_card"));
		System.out.println("CNY daifu response: " + cnyDaifuRes);
		
		// 加密币代付请求-USDT-ERC20（Erc20）
		String usdtDaifuRes = client.daifuPost(client.makeUSDTRequestParams("Erc20", "123321"));
		System.out.println("USDT daifu response: " + usdtDaifuRes);
	}
}
