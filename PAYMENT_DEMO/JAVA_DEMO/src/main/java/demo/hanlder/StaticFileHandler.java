package demo.hanlder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class StaticFileHandler extends BaseHandler implements HttpHandler {
	Logger logger = LoggerFactory.getLogger(getClass());
	String directory;

	public StaticFileHandler(String directory) {
		super();
		this.directory = directory;
	}

	protected String getFilePath(String uri) {
		return this.directory + uri.substring(this.getUrlPrefix().length() - 1);
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		String filePath = this.getFilePath(exchange.getRequestURI().getPath());
		Path path = Paths.get(filePath);

		if (!Files.exists(path)) {
			this.sendResponse(exchange, 404, "file not found".getBytes());
			return;
		}

		if (filePath.endsWith(".js")) {
			exchange.getResponseHeaders().add("Content-Type", "application/javascript");
		} else if (filePath.endsWith(".html")) {
			exchange.getResponseHeaders().add("Content-Type", "text/html");
		}
		this.sendResponse(exchange, 200, Files.readAllBytes(path));
	}
}
