package demo.lib;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Utils {
	public static Map<String, String> decode(String data) throws UnsupportedEncodingException {
		String[] paramItems = data.split("&");
		Map<String, String> params = new HashMap<>();

		for (int i = 0; i < paramItems.length; ++i) {
			String[] paramKV = paramItems[i].split("=");
			if (paramKV.length > 0) {
				params.put(URLDecoder.decode(paramKV[0], StandardCharsets.UTF_8.name()),
						URLDecoder.decode(paramKV.length > 1 ? paramKV[1] : "", StandardCharsets.UTF_8.name()));
			}
		}

		return params;
	}

	public static String encode(Map<String, String> params) throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();

		for (Entry<String, String> entry : params.entrySet()) {
			sb.append(URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8.name())).append("=")
					.append(URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8.name())).append("&");
		}
		if (sb.length() > 0) {
			sb.setLength(sb.length() - 1);
		}
		return sb.toString();
	}

	public static String hex(byte[] bytes) {
		final char[] chars = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
				'f' };
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; ++i) {
			int b = bytes[i] & 0xff;
			sb.append(chars[b >> 4]).append(chars[b & 0x0f]);
		}
		return sb.toString();
	}

	public static String md5(String data) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");

		md.update(data.getBytes());
		return hex(md.digest());
	}
}
