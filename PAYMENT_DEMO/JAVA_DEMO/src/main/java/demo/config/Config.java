package demo.config;

public class Config {
	/**
	 * 支付api域名。
	 */
	public static final String api_domain = "http://api.acepay.net";
	/**
	 * 商户密钥，也就是在商户后台看到的PSK。
	 */
	public static final String merchant_key = "";
}
